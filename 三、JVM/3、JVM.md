### 3.1、说一说JVM内存结构？栈里面存储什么数据？
答：JVM 内存区域主要分为线程私有区域【程序计数器、虚拟机栈、本地方法栈】、线程共享区 域【JAVA 堆、方法区】
![JVM内存结构](https://images.gitee.com/uploads/images/2021/0423/103311_189eb98a_2299347.jpeg "ip_image002.jpeg")

### 3.2、JVM的垃圾回收器有哪些？
答：Serial、SerialOld、ParNew、CMS、ParallelScavenge、ParallelOld、G1
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0423/103403_c9a9b550_2299347.jpeg "ip_image004.jpeg")
