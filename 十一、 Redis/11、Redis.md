### 11.1、Redis的数据结构有哪些？分别应用在哪些场景？
答：
- （1）String类型，一般用于需要计数的场景，例如：访问次数，点赞数。
- （2）Hash类型，一般用于系统对象数据的存储。
- （3）List类型，一般用于消息队列。
- （4）Set类型，一般用于不能出现重复数据的场景，例如可以非常方便的实现共同关注，共同粉丝等功能。
- （5）Zset类型，有序集合，用于需要对数据按某个权重进行排序的场景，例如：直播系统中的排行榜。
### 11.2、Redis分布式锁会出现死锁情况吗？
答：不会，因为会设置过期时间
### 11.3、Redis分布式锁的原理？
答：分布式锁的目的其实很简单，就是为了保证多台服务器在执行某一段代码时保证只有一台服务器执行。为了保证分布式锁的可用性，至少要确保锁的实现要同时满足以下几点：互斥性。在任何时刻，保证只有一个客户端持有锁。不能出现死锁。如果在一个客户端持有锁的期间，这个客户端崩溃了，也要保证后续的其他客户端可以上锁。保证上锁和解锁都是同一个客户端
### 11.4、什么是缓存穿透？有什么解决方法？
答：缓存穿透说简单点就是大量请求的 key 根本不存在于缓存中，导致请求直接到了数据库上，根本没有经过缓存这一层。
 解决方式：
- 1）缓存无效 key   
- 2）使用布隆过滤器
### 11.5、什么是缓存雪崩？怎么避免？
答：描述的就是这样一个简单的场景：缓存在同一时间大面积的失效，后面的请求都直接落到了数据库上，造成数据库短时间内承受大量请求。
解决办法：
- 1）采用Redis 集群，避免单机出现问题整个缓存服务都没办法使用
- 2）限流，避免同时处理大量的请求
- 3）设置不同的缓存失效时间

### 11.6、RedisIO模型是什么？
答：
### 11.7、Redis持久化原理？
答：
- 快照持久化（RDB）:快照持久化是Redis默认采用的持久化方式，通过创建快照来获得存储在内存里面的数据在某个时间点上的副本。
- AOF持久化：默认情况下Redis 没有开启AOF方式的持久化，可以通过 appendonly 参数开启。开启AOF持久化后每执行一条会更改Redis中的数据的命令，Redis就会将该命令写入硬盘中的AOF文件件。AOF文件的保存位置和RDB文件的位置相同，都是通过 dir 参数设置的，默认的文件名是 appendonly.aof
- 混合持久化：Redis4.0后开始支持，默认关闭，可以通过配置项 aof-use-rdbpreamble 开启，如果把混合持久化打开，AOF重写的时候就直接把RDB的内容写到AOF文件开头。这样做的好处是可以结合 RDB 和 AOF 的优点, 快速加载同时避免丢失过多的数据。当然缺点也是有的，AOF里面的RDB部分是压缩格式，不再是AOF格式，可读性较差。
### 11.8、Redis集群部署过吗？修改哪些配置？
答：部署过，redis集群需要至少三个master节点，我们是搭建三个master节点，并且给每个master再搭建一个slave节 点，总共6个redis节点